import axios from 'axios'

const clientHTTP = axios.create({
    baseURL: `http://localhost:3001`
})

clientHTTP.defaults.headers.post['Content-Type'] = 'application/json';

export {
    clientHTTP
}