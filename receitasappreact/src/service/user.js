import { clientHTTP } from '../config/config'

const createUser = (data) => clientHTTP.post('/users/new', data)

const listUser = () => clientHTTP.get('/users')

const deleteUser = (user) => clientHTTP.delete(`/users/${user.email}`)

export {
    createUser,
    listUser,
    deleteUser
}