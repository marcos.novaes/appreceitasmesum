import React from 'react';
import './App.css';
import Header from './components/layout/header'
import Footer from './components/layout/footer'
import Routes from './routes'
import {
  BrowserRouter as Roteador
} from "react-router-dom";


function App() {


  return (
    <React.Fragment>
      <Roteador>
        <Header />
        <main>
          <Routes />
        </main>
      </Roteador>
      <Footer></Footer>
    </React.Fragment>
  );
}

export default App;
