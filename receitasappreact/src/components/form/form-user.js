import React, { useState } from 'react'
import { createUser } from '../../service/user'
import { useHistory } from 'react-router-dom'

const FormUser = ({ mudaPagina }) => {

    const [form, setForm] = useState({})
    const history = useHistory()

    const handleChange = (event) => {
        setForm({
            ...form,
            [event.target.name]: event.target.value
        })
        return
    }

    const formIsPreenchido = () => {
        return form.nome && form.email && form.telefone && form.senha && form.senhaConfirmacao
    }


    const submitForm = async (event) => {
        try {
            event.preventDefault()
            await createUser(form)
            setForm({})
            alert('formulario enviado com sucesso')
            history.push('/users')
        } catch (error) {
            console.log(error.message)
        }
    }


    return (
        <div className="form">
            <h2>Formulário de cadastro</h2>
            <form>
                <div className="form-group">
                    <label htmlFor="nome">Nome:</label>
                    <input onChange={(e) => handleChange(e)} value={form.nome || ""} type="text" name="nome" id="inputName" />
                </div>
                <div className="form-group">
                    <label htmlFor="email">Email:</label>
                    <input onChange={(e) => handleChange(e)} value={form.email || ""} type="text" name="email" id="inputEmail" />
                </div>
                <div className="form-group">
                    <label htmlFor="telefone">Telefone:</label>
                    <input onChange={(e) => handleChange(e)} value={form.telefone || ""} type="text" name="telefone" id="inputTel" />
                </div>
                <div className="form-group">
                    <label htmlFor="senha">Senha:</label>
                    <input onChange={(e) => handleChange(e)} value={form.senha || ""} type="password" name="senha" id="inputSenha" />
                </div>
                <div className="form-group">
                    <label htmlFor="senhaConfirmacao">Confirmacao de Senha:</label>
                    <input onChange={(e) => handleChange(e)} value={form.senhaConfirmacao || ""} type="password" name="senhaConfirmacao" id="inputSenha" />
                </div>

                <div className="form-group"><button onClick={(e) => submitForm(e)} disabled={!formIsPreenchido()} className="buttonSubmit">Enviar!</button></div>
            </form>
        </div>
    )

}

export default FormUser