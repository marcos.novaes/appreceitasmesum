import React, { useState } from 'react'
import Ingredient from './ingredient'
import Passo from './passo'
import { createRecipe } from '../../service/recipe'
import { useHistory } from 'react-router-dom'

const FormReceita = ({ mudaPagina }) => {
    const history = useHistory()
    const [listIngredientes, setListIngredientes] = useState([])
    const [listPassos, setListPassos] = useState([])
    const [form, setForm] = useState({
        "tipo": "salgado",
        "dificuldade": "1"
    })

    const incluirIng = (valor) => setListIngredientes([
        ...listIngredientes,
        valor
    ])


    const removeIng = (i) => {
        listIngredientes.splice(i, 1)
        setListIngredientes([
            ...listIngredientes
        ])
    }

    const incluirPasso = (valor) => setListPassos([
        ...listPassos,
        valor
    ])

    const removePasso = (i) => {
        listPassos.splice(i, 1)
        setListPassos([
            ...listPassos
        ])
    }

    const handleChange = (e) => {
        setForm({
            ...form,
            [e.target.name]: e.target.value
        })
        return
    }

    const formIsValid = () => {
        return (form.nome && form.rendimento && form.tempo && listPassos.length > 0 && listIngredientes.length > 0)
    }

    const montaBody = () => {
        let body = {
            ...form
        }
        body["ingredientes"] = listIngredientes
        body["passos"] = listPassos

        return body
    }

    const zeraForm = () => {
        setForm({
            "tipo": "salgado",
            "dificuldade": "1"
        })
        setListIngredientes([])
        setListPassos([])
    }

    const submitForm = async (e) => {
        e.preventDefault()
        try {
            const reqBody = montaBody()
            await createRecipe(reqBody)
            zeraForm()
            alert('formulario enviado com sucesso')
            history.push('/recipes')
        } catch (error) {
            console.log(error.message)
        }

    }

    return (
        <div className="form">
            <h2>Preencha com as informaçoes da sua receita</h2>
            <form >
                <div className="form-group">
                    <label htmlFor="nome">Nome da receita:</label>
                    <input onChange={(e) => handleChange(e)} value={form.nome || ""} type="text" name="nome" id="inputName" />
                </div>
                <div className="form-group">
                    <label htmlFor="tipo">Categoria:</label>
                    <select onChange={(e) => handleChange(e)} name="tipo" id="inputCategoria">
                        <option value="salgado">Salgado</option>
                        <option value="doce">Doce</option>
                    </select>
                </div>
                <div className="form-group">
                    <label htmlFor="rendimento">Rendimento(porçoes):</label>
                    <input onChange={(e) => handleChange(e)} value={form.rendimento || ""} type="number" name="rendimento" id="inputRendimento" />
                </div>
                <div className="form-group">
                    <label htmlFor="tempo">Tempo de preparo (minutos):</label>
                    <input onChange={(e) => handleChange(e)} value={form.tempo || ""} type="number" name="tempo" id="inputTempo" />
                </div>
                <div className="form-group">
                    <label htmlFor="dificuldade">Dificuldade:</label>
                    <select onChange={(e) => handleChange(e)} name="dificuldade" id="inputDificuldade">
                        <option value="1">1 ( Muito facil )</option>
                        <option value="2">2 ( facil )</option>
                        <option value="3">3 ( Normal )</option>
                        <option value="4">4 ( Dificil )</option>
                        <option value="5">5 ( Muito dificil )</option>
                    </select>
                </div>
                <div className="form-group">
                    <label htmlFor="ingredientes">Ingredientes:</label>
                    <div id="ingredientes">

                        <Ingredient addIng={incluirIng}></Ingredient>
                        {listIngredientes.length > 0 ? listIngredientes.map((item, index) => (
                            <div key={index}>
                                {(index + 1) + ". " + item}
                                <button type="button" className="removeItem" onClick={() => removeIng(index)} >-</button>
                            </div>
                        )) : ""}
                    </div>
                </div>
                <div className="form-group">
                    <label htmlFor="passos">Como fazer(Digite passo a passo): </label>
                    <div id="passos">
                        <Passo addPasso={incluirPasso}></Passo>
                        {listPassos.length > 0 ? listPassos.map((item, index) => (
                            <div key={index}>
                                {(index + 1) + ". " + item}
                                <button type="button" className="removeItem" onClick={() => removePasso(index)} >-</button>
                            </div>
                        )) : ""}
                    </div>
                </div>

                <div className="form-group"><button onClick={(e) => submitForm(e)} disabled={!formIsValid()} className="buttonSubmit">Enviar!</button></div>
            </form>
        </div>
    )
}

export default FormReceita