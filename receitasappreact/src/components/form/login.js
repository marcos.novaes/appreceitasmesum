import React, { useState } from 'react'


const Login = () => {
    const [form, setForm] = useState({})

    const handleChange = (event) => {
        setForm({
            ...form,
            [event.target.name]: event.target.value
        })
        return
    }

    const formIsValid = () => {
        return form.email && form.senha
    }

    const submitForm = (e) => {
        e.preventDefault()
        console.log(form)
    }

    return (
        <div className="form">
            <form>
                <h2>LOGIN</h2>
                <div className="form-group">
                    <label htmlFor="email">Email:</label>
                    <input onChange={(e) => handleChange(e)} type="text" value={form.email || ""} name="email" id="inputEmail" />
                </div>
                <div className="form-group">
                    <label htmlFor="senha">Senha:</label>
                    <input onChange={(e) => handleChange(e)} value={form.senha || ""} type="password" name="senha" id="inputSenha" />
                </div>
                <button onClick={(e) => submitForm(e)} disabled={!formIsValid()} className="buttonSubmit">Enviar!</button>
            </form>
        </div>
    )
}

export default Login