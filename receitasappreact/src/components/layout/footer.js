import React from 'react'

const Footer = () => {

    return (
        <footer>
            <h2>FEITO POR MARCOS NOVAES</h2>
            <a href="https://github.com/marcosnovaesq">Github</a>
            <a href="https://www.linkedin.com/in/marcos-novaes-006202136/">Linkedin</a>
        </footer>
    )


}

export default Footer