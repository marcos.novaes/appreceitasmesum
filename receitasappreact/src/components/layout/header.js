import React from 'react'
import logoFarofa from '../../assets/img/default2.svg'
import { useHistory } from 'react-router-dom'


const Header = () => {
    const history = useHistory()
    const changePage = (value) => {
        history.push(value)
    }

    return (
        <header>
            <div className="logo">
                <img onClick={() => changePage('/home')} src={logoFarofa} alt="logo da farofinha" />
            </div>
            <div className="menu">
                <div className="search">
                    <input type="search" name="search" id="search" placeholder="Procure aqui a receita!" />
                    <button type="submit"><i className="fa fa-search"></i></button>
                </div>
                <div className="nav">
                    <button onClick={() => changePage('/recipes/create')} >Crie sua receita!</button>
                    <button onClick={() => changePage('/recipes')} >Todas as receitas</button>
                    <button onClick={() => changePage('/users/create')} > Cadastre-se</button>
                    <button onClick={() => changePage('/')} >Login</button>
                    <button onClick={() => changePage('/users')} >Usuários</button>
                </div>
            </div>

        </header>
    )



}

export default Header