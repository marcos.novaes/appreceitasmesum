import React from 'react'
import FormReceita from './components/form/form-receita'
import ListReceitas from './components/list/list-receitas';
import FormUser from './components/form/form-user'
import ListUser from './components/list/list-user'
import Login from './components/form/login'
import IndexSection from './components/layout/index-section'
import {
    Switch,
    Route
} from "react-router-dom";



const Routes = () => (

    <Switch>
        <Route exact path="/" component={Login} />
        <Route exact path="/recipes/create" component={FormReceita} />
        <Route exact path="/users/create" component={FormUser} />
        <Route exact path="/recipes" component={ListReceitas} />
        <Route exact path="/users" component={ListUser} />
        <Route exact path="/home" component={IndexSection} />
        <Route exact path="*" component={() => (<h1>404 | Not Found</h1>)} />
    </Switch>

)

export default Routes;




// const routes = {
//     'Index': {
//         component: IndexSection
//     },
//     'FormReceitas': {
//         component: FormReceita
//     },
//     'ListReceitas': {
//         component: ListReceitas
//     },
//     'FormUsers': {
//         component: FormUser
//     },
//     'ListUsers': {
//         component: ListUser
//     },
//     'Login': {
//         component: Login
//     }
// }

// export default routes