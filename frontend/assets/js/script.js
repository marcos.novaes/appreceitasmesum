const urlGetListServer = 'http://localhost:3000/users'

const tbody = document.querySelector('#table table tbody')

const createElement = (item) => {
    const element = document.createElement("tr")
    element.innerHTML = `
        <td>${item.nome}</td>
        <td>${item.email}</td>
        <td>${item.telefone}</td>
        <td>${item.senha}</td>
    `

    return element
}

async function getListUser() {
    const result = await fetch(urlGetListServer)
    const data = await result.json()

    data.map(item => tbody.appendChild(createElement(item)))
}




function addIngrediente(event) {

    event.preventDefault();
    const divIngredientes = document.getElementById("ingredientes");
    const element = document.createElement("div")
    element.className = "divIngrediente"
    element.innerHTML = `<input type="text" name="ingredientes" class="inputIngredientes">
    <button type="button" class="buttonIngrediente" onclick="addIngrediente(event)">+</button>
    <button type="button" class="buttonIngrediente" onclick="removeIngrediente(event)">-</button>`
    divIngredientes.appendChild(element)
}

function removeIngrediente(event) {
    event.preventDefault()
    const divIngredientes = document.getElementById("ingredientes");
    var array = Array.from(divIngredientes.childNodes);
    const arrayelementos = array.filter((val) => val.innerHTML !== undefined)
    if (arrayelementos.length >= 2) {
        const elemento = event.srcElement.parentElement
        divIngredientes.removeChild(elemento)
    }
}

function addPasso(event) {

    event.preventDefault();
    const divPassos = document.getElementById("passos");
    const element = document.createElement("div")
    element.className = "divPassos"
    element.innerHTML = `<input type="text" name="passos" class="inputPassos">
    <button type="button" class="buttonPassos btnAdd" onclick="addPasso(event)">+</button>
    <button type="button" class="buttonPassos btnRemove" onclick="removePasso(event)">-</button>`
    divPassos.appendChild(element)
}

function removePasso(event) {
    event.preventDefault()
    const divPassos = document.getElementById("passos");
    var array = Array.from(divPassos.childNodes);
    const arrayelementos = array.filter((val) => val.innerHTML !== undefined)
    if (arrayelementos.length >= 2) {
        const elemento = event.srcElement.parentElement
        divPassos.removeChild(elemento)
    }
}

function carregaUsuarios() {
    // const result = await fetch('http://localhost:3000/users')
    // const data = await result.json()
    console.log('shudhus')
}